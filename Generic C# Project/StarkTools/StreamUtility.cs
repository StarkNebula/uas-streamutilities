﻿// Created by Raphael "Stark" Tetreault 01/06/2016
// Copyright © 2016 Raphael Tetreault

using System.Collections.Generic;

namespace System.IO
{
    /// <summary>
    /// A suite of methods to simplify the task of creating and managing smaller streams.
    /// </summary>
    /// <remarks>
    /// This library was not designed for, tested, or intended to be used with large streams. It is
    /// recommended to keep streams under 10MB.
    /// </remarks>
    public static class StreamUtility
    {
        /// <summary>
        /// Indicates the byte order ("endianness") in which data is written to the stream.
        /// </summary>
        /// <remarks>
        /// This field is set to <c>false</c> by default.
        /// </remarks>
        public static bool isLittleEndian = false;

        /// <summary>
        /// Writes a binary stream of all the <c>System.ValueType</c> or <c>string</c> value types passed to the method in order
        /// in the endianness specified by <see cref="isLittleEndian"/>.
        /// </summary>
        /// 
        /// <param name="data">
        /// Any keyword data type, multidimensional <c>[ , ]</c> or jagged <c>[ ][ ]</c> array of keyword data type, or list of keyword
        /// data type, including any combination of the aforementioned types, such as lists of jagged arrays.
        /// </param>
        /// 
        /// <remarks>
        /// Valid value types:
        /// <list type="bullet">
        ///   <item>bool</item>
        ///   <item>char</item>
        ///   <item>string</item>
        ///   <item>sbyte</item>
        ///   <item>byte</item>
        ///   <item>short</item>
        ///   <item>ushort</item>
        ///   <item>int</item>
        ///   <item>uint</item>
        ///   <item>long</item>
        ///   <item>ulong</item>
        ///   <item>float</item>
        ///   <item>double</item>
        ///   <item>decimal</item>
        /// </list>
        /// </remarks>
        /// 
        /// <example>
        /// <code>
        /// Stream binaryStream = new MemoryStream();
        ///
        /// float single   = 3.14f;
        /// int[] intArray = new int[] { 1, 2, 3, 4 };
        /// string str     = "Hello world!";
        ///
        /// binaryStream = StreamUtility.MakeBinaryStream( single, intArray, str );
        ///
        /// binaryStream.DebugStream();
        ///
        /// // DebugStream Output:
        /// // 0x00 - 4048F5C3 00000001 00000002 00000003 - [@HõÃ            ]
        /// // 0x10 - 00000004 48656C6C 6F20776F 726C6421 - [    Hello world!]
        /// </code>
        /// </example>
        /// <exception cref="System.IO.IOException">
        /// One or more of the arguments passed is not of a keyword data type.
        /// </exception>
        public static Stream MakeBinaryStream(params dynamic[] data)
        {
            BinaryWriter writer = new BinaryWriter(new MemoryStream());
            RecursiveDynamicDataWriter(ref writer, data);

            //Console.WriteLine("Final stream length = dec:{0} hex:{1}", writer.BaseStream.Length, writer.BaseStream.Length.ToString("X"));
            //writer.BaseStream.DebugStream();

            return writer.BaseStream;
        }
        /// <summary>
        /// Writes dynamicData to the reference BinaryWriter. If any part of dynamicData is an array
        /// (multidimensional <c>[ , ]</c> or jagged <c>[ ][ ]</c>) or a list, including lists of the aforementioned types,
        /// it will recursively break it down to it's most basic data component then serialize it.
        /// </summary>
        /// <param name="binaryWriter">The <c>BinaryReader</c> to write the <paramref name="dynamicData"/> to.</param>
        /// <param name="dynamicData">The data to write to the <c>BinaryWriter</c>.</param>
        private static void RecursiveDynamicDataWriter(ref BinaryWriter binaryWriter, params dynamic[] dynamicData)
        {
            // It is important to know that a dynamic[] is really just a special case object[].
            // This means that what we are receiving from the method is a boxed variable type.
            // https://msdn.microsoft.com/en-us/library/yz2be5wk.aspx?f=255&MSPPError=-2147217396
            //
            // The thing to note is that what we wish to do in this method is unbox the object
            // into it's smallest possible value type, such as int, float, etc., so that we may
            // serialize it in it's binary form with BitConverter. To do so we must unbox the
            // object[], then if we have an array or list, "unbox" it into its individual
            // components. To make this truly useful, we would need the method to know how deeply
            // to unbox the type. That is why this method is recursive; it will detect when it
            // needs to unbox the item again. However, in doing so we end up reboxing the data into
            // an object[] once more. This means we must unpack the object[], then unbox the array
            // or list before passing it to recursive method again, otherwise we would just end up
            // unboxing the value, checking if it is an array or list, and then immediately
            // reboxing it.
            //
            // We can check the data type by using Type.IsArray for any type of
            // array (jagged [][] or multidimensional [,]) or Type.IsGenericType for lists. I 
            // use a foreach loop twice; once to unbox the object[], then again if necessary
            // to unbox the array or list. At this point we know that the method will handle
            // any boxed type so we can then proceed to serializing the data, AKA the unboxed
            // keyword types like int, float, decimal, and char. I added a special case for
            // strings too, although they technically aren't a System.ValueType, which are
            // the only types BinaryWriter support.
            //
            // At this point you should be able to follow along with the code/comments below.

            // Foreach object in object[]
            foreach (dynamic item in dynamicData)
            {
                // If the unboxed type is an array
                if (GetTypeOf(item).IsArray)
                    // Unbox the array further
                    foreach (dynamic subItem in item)
                        // Pass each unboxed item back to this method
                        RecursiveDynamicDataWriter(ref binaryWriter, subItem);

                // If the unboxed type is a list
                else if (GetTypeOf(item).IsGenericType)
                    // Unbox the list further
                    foreach (dynamic subItem in item)
                        // Pass each unboxed item back to this method
                        RecursiveDynamicDataWriter(ref binaryWriter, subItem);

                // Prevent any array or list from proceeding, as we cannot handle it. It
                // will have been broken down further and the passed through this method
                // again, meaning this array's/list's component will pass this gate.
                if (GetTypeOf(item).IsArray || GetTypeOf(item).IsGenericType)
                    goto EndOfForeachLoop;

                // Check the type of the object and handle it accordingly
                //
                // STRING
                // BinaryWriter does not support strings. We can however pass in a char[].
                if (GetTypeOf(item) == typeof(string))
                    binaryWriter.Write(((string)item).ToCharArray());

                // BYTE - SBYTE
                // If we let this fall into BitConverter, bytes/sbytes get casted into shorts!
                // Instead we can just write the bytes/sbytes to stream as endianness doesn't
                // affect writing single bytes/sbytes.
                else if (GetTypeOf(item) == typeof(byte) || GetTypeOf(item) == typeof(sbyte))
                    binaryWriter.Write(item);

                // DECIMAL
                // BitConverter does not support decimals. We can get around this with by using
                // a BinaryWriter. The issue now is supporting endianness. A BinaryWriter will
                // write in Little-Endian by default. To support Big-Endian, we need to do a little
                // detour. We can write it to a temporary stream, get the bytes from it and reverse
                // them, then write the reversed array to our target stream.
                else if (GetTypeOf(item) == typeof(decimal))
                {
                    BinaryWriter tempStream = new BinaryWriter(new MemoryStream());
                    // Write decimal to stream
                    tempStream.Write(item);
                    byte[] decimalBytes = tempStream.BaseStream.StreamToByteArray();

                    // Reverse only if requesting Little-Endian
                    if (isLittleEndian) Array.Reverse(decimalBytes);

                    binaryWriter.Write(decimalBytes);
                }

                // EVERYTHING ELSE
                // bool, char, short, ushort, int, uint, long, ulong, float, double
                else
                {
                    try
                    {
                        // Get data as bytes. BitConverter will accept the remaining types
                        // assuming they are not structs, classes, or interfaces. If so, it
                        // will throw an error that will get caught.
                        byte[] data = BitConverter.GetBytes(item);

                        // Write in the endianness desired. We only want to reverse the
                        // array when the BitConverter's endianness does not match the
                        // desired endianness. Thus we XOR (^) the bools. When these two
                        // endiannesses don't match, XOR returns true as it means we need
                        // to correct the written endianness.
                        // Want Big-Endian but BitConverter is Little-Endian? Reverse.
                        // Want Little-Endian but BitConverter is Big-Endian? Reverse.
                        if (BitConverter.IsLittleEndian ^ isLittleEndian)
                            Array.Reverse(data);

                        binaryWriter.Write(data);
                    }
                    catch
                    {
                        // Catch any type that got passed that is not a Value Type or String, such
                        // as structs, classes, interfaces or other. Throw error with value name.
                        throw new IOException(
                            "The type " + GetTypeOf(item) + " is not supported! (M:MakeBinaryStream)"
                            );
                    } // End of try/catch
                } // End of else (everything else)
            EndOfForeachLoop:;
            } // End of foreach
        } // End of method
        /// <summary>
        /// Used to retrieve the type of a <c>dynamic</c> as (object) casts become plenty very quickly.
        /// </summary>
        /// <param name="data">Type of dynamic.</param>
        /// <returns></returns>
        private static Type GetTypeOf(dynamic data)
        {
            return ((object)data).GetType();
        }

        /// <summary>
        /// Combines all the streams passed in order into a single <c>Stream</c>.
        /// </summary>
        /// <param name="streams">Any number of streams to be merged into a single stream.</param>
        public static Stream ConcatenateStreams(params Stream[] streams)
        {
            return ConcatenateStreamsToByteArray(streams).ByteArrayToStream();
        }
        /// <summary>
        /// Combines all the streams passed in order to a single <c>byte[]</c>.
        /// </summary>
        /// <param name="streams">Any number of streams to be merged into a single byte array.</param>
        public static byte[] ConcatenateStreamsToByteArray(params Stream[] streams)
        {
            List<byte> bytes = new List<byte>();

            foreach (Stream stream in streams)
                bytes.AddRange(stream.StreamToByteArray());

            return bytes.ToArray();
        }

        /// <summary>
        /// Saves the current stream to a file at the specified <paramref name="path"/> which
        /// includes the filename and extension.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="path">The directory to place the new file in with filename and extension.</param>
        /// <remarks>
        /// This method uses File.Create, which means it will overwrite a file of the same name
        /// if they are in the same directory.
        /// </remarks>
        public static void SerializeStream(Stream stream, string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create);
            fs.Write(stream.StreamToByteArray(), 0, (int)stream.Length);
            fs.Close();
            fs.Dispose();
        }
        /// <summary>
        /// Saves the current stream to a file at <paramref name="directory"/> of name 
        /// and extension <paramref name="filenameAndExtension"/>.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="directory">The directory to place the new file in.</param>
        /// <param name="filenameAndExtension">The name of the file with extension to be created.</param>
        /// <remarks>
        /// This method uses File.Create, which means it will overwrite a file of the same name
        /// if they are in the same directory.
        /// </remarks>
        public static void SerializeStream(Stream stream, string directory, string filenameAndExtension)
        {
            SerializeStream(stream, Path.Combine(directory, filenameAndExtension));
        }
        /// <summary>
        /// Saves the current stream to a file at <paramref name="directory"/> of name 
        /// <paramref name="filename"/> with the specified <paramref name="extension"/>.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="directory">The directory to place the new file in.</param>
        /// <param name="filename">The name of the file to be created.</param>
        /// <param name="extension">The extension of the new file without a prepended period.</param>
        /// <remarks>
        /// This method uses File.Create, which means it will overwrite a file of the same name
        /// if they are in the same directory.
        /// </remarks>
        public static void SerializeStream(Stream stream, string directory, string filename, string extension)
        {
            SerializeStream(stream, Path.Combine(directory, filename + "." + extension));
        }
    }
}