﻿// Created by Raphael "Stark" Tetreault 01/06/2016
// Copyright © 2016 Raphael Tetreault

namespace System.IO
{
    /// <summary>
    /// A suite of extensions for <c>System.IO.Stream</c> which simplify converting and debugging streams.
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// Returns the byte representation of the current stream as a <c>byte[]</c>.
        /// </summary>
        /// <param name="stream">The current Stream to read as a byte array.</param>
        public static byte[] StreamToByteArray(this Stream stream)
        {
            return (stream as MemoryStream).ToArray();
        }
        /// <summary>
        /// Returns the byte representation of the current FileStream's BaseStream as a <c>byte[]</c>.
        /// </summary>
        /// <param name="fileStream">The current FileStream to read as a byte array.</param>
        public static byte[] StreamToByteArray(this FileStream fileStream)
        {
            // If a FileStream were passed as a Stream (explicit cast), the above method
            // "public static byte[] StreamToByteArray(this Stream stream)" would throw
            // a NullReferenceException. To circumvent that, simply copy the FileStream
            // to a MemoryStream (or stream). In this case a MemoryStream to return a
            // byte[] using MemoryStream.ToArray().

            MemoryStream memoryStream = new MemoryStream();
            fileStream.CopyTo(memoryStream);

            return memoryStream.ToArray();
        }

        /// <summary>
        /// Returns the stream representation of the current byte array as a <c>Stream</c>. 
        /// </summary>
        /// <param name="bytes">The current byte array.</param>
        public static Stream ByteArrayToStream(this byte[] bytes)
        {
            return new MemoryStream(bytes);
        }

        /// <summary>
        /// Returns the current FileStream's BaseStream as a <c>Stream</c>.
        /// </summary>
        /// <param name="fileStream">The current file stream.</param>
        /// <remarks>
        /// While in some cases C# allows you to implicitly cast a <c>FileStream</c> to <c>Stream</c>,
        /// it may cause a <c>NullReferenceException</c>. This ensure that by explicitly casting
        /// it such a case will not occur.
        /// </remarks>
        public static Stream FileStreamToStream(this FileStream fileStream)
        {
            MemoryStream memoryStream = new MemoryStream();
            fileStream.CopyTo(memoryStream);

            return memoryStream;
        }

        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        public static void DebugStream(this Stream stream)
        {
            stream.DebugStream(4, 16, "X2");
        }
        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="hexFormat">The display format of the offset indicator.</param>
        /// <remarks>
        /// The format printed in is variable as defined by the parameters. The organization
        /// of data is akin to that of HxD, a popular hex editor program.
        /// </remarks>
        public static void DebugStream(this Stream stream, string hexFormat)
        {
            stream.DebugStream(4, 16, hexFormat);
        }
        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="byteGroup">The cluster of byte values, separated by a space.</param>
        /// <param name="bytesPerRow">the number of bytes each line, separated by a newline.</param>
        /// <remarks>
        /// The format printed in is variable as defined by the parameters. The organization
        /// of data is akin to that of HxD, a popular hex editor program.
        /// </remarks>
        public static void DebugStream(this Stream stream, uint byteGroup, uint bytesPerRow)
        {
            stream.DebugStream(byteGroup, bytesPerRow, "X2");
        }
        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="byteGroup">The cluster of byte values, separated by a space.</param>
        /// <param name="bytesPerRow">the number of bytes each line, separated by a newline.</param>
        /// <param name="hexFormat">The display format of the offset indicator.</param>
        /// <remarks>
        /// The format printed in is variable as defined by the parameters. The organization
        /// of data is akin to that of HxD, a popular hex editor program.
        /// </remarks>
        public static void DebugStream(this Stream stream, uint byteGroup, uint bytesPerRow, string hexFormat)
        {
            string outputLine = null;
            string bytesAsString = null;
            int count = 0;

            foreach (byte b in stream.StreamToByteArray())
            {
                // Prepend "0xPOS" to the start of each new console line
                if (count % bytesPerRow == 0)
                    outputLine += "0x" + count.ToString("X2") + " - ";

                count++;
                outputLine += (b.ToString(hexFormat));
                bytesAsString += (char)b;

                // Insert a space every byteGroup's apart.
                if (count % byteGroup == 0)
                    outputLine += " ";

                // Append string representation of bytes
                if (count % bytesPerRow == 0)
                {
                    outputLine += "- [" + bytesAsString + "]\n";

                    // Clear string for next loop
                    bytesAsString = null;
                }
            }

            // Print outputLine to console
            if (bytesAsString != null)
                outputLine += " - [" + bytesAsString + "]\n";

            // Print this line to Console
            Console.WriteLine(outputLine);
        }
    }
}