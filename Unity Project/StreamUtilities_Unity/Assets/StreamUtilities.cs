﻿using System.Collections.Generic;

namespace System.IO
{
    /// <summary>
    /// A suite of methods to simplify the task of creating and managing streams.
    /// </summary>
    public static class StreamUtilities
    {
        /// <summary>
        /// Writes a binary stream of all the <c>System.ValueType</c> or <c>string</c> value types passed to the method, in order.
        /// </summary>
        /// 
        /// <param name="data">
        /// Any keyword data type, multidimensional <c>[ , ]</c> or jagged <c>[ ][ ]</c> array of keyword data type, or list of keyword
        /// data type, including any combination of the aforementioned types, such as lists of jagged arrays.
        /// </param>
        /// 
        /// <remarks>
        /// Valid value types:
        /// <list type="bullet">
        ///   <item>bool</item>
        ///   <item>char</item>
        ///   <item>string</item>
        ///   <item>sbyte</item>
        ///   <item>byte</item>
        ///   <item>short</item>
        ///   <item>ushort</item>
        ///   <item>int</item>
        ///   <item>uint</item>
        ///   <item>long</item>
        ///   <item>ulong</item>
        ///   <item>float</item>
        ///   <item>double</item>
        ///   <item>decimal</item>
        /// </list>
        /// </remarks>
        /// 
        /// <example>
        /// <code>
        /// Stream binaryStream = new MemoryStream();
        ///
        /// float single   = 3.14f;
        /// int[] intArray = new int[] { 1, 2, 3, 4 };
        /// string str     = "Hello world!";
        ///
        /// binaryStream = StreamUtilities.MakeBinaryStream( single, intArray, str );
        ///
        /// binaryStream.DebugStream();
        ///
        /// // DebugStream Output:
        /// // 0x00 - 4048F5C3 00000001 00000002 00000003 - [@HõÃ            ]
        /// // 0x10 - 00000004 48656C6C 6F20776F 726C6421 - [    Hello world!]
        /// </code>
        /// </example>
        public static Stream MakeBinaryStream(params dynamic[] data)
        {
            BinaryWriter writer = new BinaryWriter(new MemoryStream());
            //RecursiveDynamicDataWriter(1, ref writer, data);
            RecursiveDynamicDataWriter(ref writer, data);

            //Console.WriteLine("Final stream length = dec:{0} hex:{1}", writer.BaseStream.Length, writer.BaseStream.Length.ToString("X"));
            //writer.BaseStream.DebugStream();

            return writer.BaseStream;
        }
        /// <summary>
        /// Writes dynamicData to the reference BinaryWriter. If any part of dynamicData is an array
        /// (multidimensional <c>[ , ]</c> or jagged <c>[ ][ ]</c>) or a list, including lists of the aforementioned types,
        /// it will recursively break it down to it's most basic data component then serialize it.
        /// </summary>
        /// <param name="binaryWriter">The <c>BinaryReader</c> to write the <paramref name="dynamicData"/> to.</param>
        /// <param name="dynamicData">The data to write to the <c>BinaryWriter</c>.</param>
        private static void RecursiveDynamicDataWriter(ref BinaryWriter binaryWriter, params dynamic[] dynamicData)
        {
            foreach (dynamic item in dynamicData)
            {
                if (GetTypeOf(item).IsArray)
                    foreach (dynamic subItem in item)
                        RecursiveDynamicDataWriter(ref binaryWriter, subItem);

                else if (GetTypeOf(item).IsGenericType)
                    foreach (dynamic subItem in item)
                        RecursiveDynamicDataWriter(ref binaryWriter, subItem);

                if (!GetTypeOf(item).IsArray)
                    if (!GetTypeOf(item).IsGenericType)
                        continue;

                if (GetTypeOf(item) == typeof(string))
                    binaryWriter.Write(((string)item).ToCharArray());
                else if (GetTypeOf(item) == typeof(byte) || GetTypeOf(item) == typeof(sbyte))
                    binaryWriter.Write(item);
                else if (GetTypeOf(item) == typeof(decimal))
                {
                    BinaryWriter tempStream = new BinaryWriter(new MemoryStream());
                    tempStream.Write(item);
                    byte[] decimalBytes = tempStream.BaseStream.StreamToByteArray();
                    Array.Reverse(decimalBytes);

                    binaryWriter.Write(decimalBytes);
                }
                else
                {
                    try
                    {
                        byte[] data = BitConverter.GetBytes(item);
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(data);

                        binaryWriter.Write(data);
                    }
                    catch
                    {
                        // Catch any type that got passed that is not a Value Type or String
                        Console.WriteLine("(!) The type {0} is not supported. Skipping...", GetTypeOf(item));
                    }
                }
            }
        }
        /// <summary>
        /// Used to retrieve the type of a <c>dynamic</c> as (object) casts become plenty very quickly.
        /// </summary>
        /// <param name="data">Type of dynamic.</param>
        /// <returns></returns>
        private static Type GetTypeOf(dynamic data)
        {
            return ((object)data).GetType();
        }

        /// <summary>
        /// Combines all the streams passed in order into a single <c>Stream</c>.
        /// </summary>
        /// <param name="streams">Any number of streams to be merged into a single stream.</param>
        public static Stream ConcatenateStreams(params Stream[] streams)
        {
            return ConcatenateStreamsToByteArray(streams).ByteArrayToStream();
        }
        /// <summary>
        /// Combines all the streams passed in order to a single <c>byte[]</c>.
        /// </summary>
        /// <param name="streams">Any number of streams to be merged into a single byte array.</param>
        public static byte[] ConcatenateStreamsToByteArray(params Stream[] streams)
        {
            List<byte> bytes = new List<byte>();

            foreach (Stream stream in streams)
                bytes.AddRange(stream.StreamToByteArray());

            return bytes.ToArray();
        }
    }
}