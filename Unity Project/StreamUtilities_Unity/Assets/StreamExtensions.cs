﻿using System.Collections.Generic;

namespace System.IO
{
    /// <summary>
    /// A suite of extensions for <c>System.IO.BinaryReader</c> which enables reading a Stream in either Big-Endian and Little-Endian ('endianess').
    /// </summary>
    public static class StreamExtensions
    {
        // Stream to...
        /// <summary>
        /// Returns the byte representation of the current stream as a <c>byte[]</c>.
        /// </summary>
        /// <param name="stream">The current Stream to read as a byte array.</param>
        public static byte[] StreamToByteArray(this Stream stream)
        {
            return (stream as MemoryStream).ToArray();
        }
        /// <summary>
        /// Returns the byte representation of the current FileStream's BaseStream as a <c>byte[]</c>.
        /// </summary>
        /// <param name="fileStream">The current FileStream to read as a byte array.</param>
        public static byte[] StreamToByteArray(this FileStream fileStream)
        {
            // If a FileStream were passed as a Stream (explicit cast), the above method
            // "public static byte[] StreamToByteArray(this Stream stream)" would throw
            // a NullReferenceException. To circumvent that, simply copy the FileStream
            // to a MemoryStream (or stream). In this case a MemoryStream to return a
            // byte[] using MemoryStream.ToArray().

            MemoryStream memoryStream = new MemoryStream();
            fileStream.CopyTo(memoryStream);

            return memoryStream.ToArray();
        }
        /// <summary>
        /// Returns the string represenation (UTF8) of the current stream.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        public static string StreamToString(this Stream stream)
        {
            BinaryReader binaryReader = new BinaryReader(stream);
            return binaryReader.GetString((int)stream.Length);
        }

        // String to...
        /// <summary>
        /// Returns the byte representation the current string as a <c>byte[]</c>.
        /// </summary>
        /// <param name="str">The current string.</param>
        public static byte[] StringToByteArray(this string str)
        {
            List<byte> bytes = new List<byte>();

            foreach (char c in str)
                bytes.Add((byte)c);

            return bytes.ToArray();
        }
        /// <summary>
        /// Returns the stream representation the current string.
        /// </summary>
        /// <param name="str">The current string.</param>
        public static Stream StringToStream(this string str)
        {
            return new MemoryStream(str.StringToByteArray());
        }

        // Byte[] to...
        /// <summary>
        /// Returns the stream representation of the current byte array as a <c>Stream</c>. 
        /// </summary>
        /// <param name="bytes">The current byte array.</param>
        public static Stream ByteArrayToStream(this byte[] bytes)
        {
            return new MemoryStream(bytes);
        }
        /// <summary>
        /// Returns the string representation of the current byte array. 
        /// </summary>
        /// <param name="bytes">The current byte array.</param>
        public static string ByteArrayToString(this byte[] bytes)
        {
            string str = null;

            foreach (byte b in bytes)
                str += (char)b;
                    
            return str;
        }

        /// <summary>
        /// Returns the current FileStream's BaseStream as a <c>Stream</c>.
        /// </summary>
        /// <param name="fileStream">The current file stream.</param>
        /// <remarks>
        /// While in some cases C# allows you to implicitly cast a <c>FileStream</c> to <c>Stream</c>,
        /// it may cause a <c>NullReferenceException</c>. This ensure that by explicitly casting
        /// it such a case will not occur.
        /// </remarks>
        public static Stream FileStreamToStream(this FileStream fileStream)
        {
            MemoryStream memoryStream = new MemoryStream();
            fileStream.CopyTo(memoryStream);

            return memoryStream;
        }

        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        public static void DebugStream(this Stream stream)
        {
            stream.DebugStream(4, 16, "X2");
        }
        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="hexFormat">The display format of the offset indicator.</param>
        /// <remarks>
        /// The format printed in is variable as defined by the parameters. The organization
        /// of data is akin to that of HxD, a popular hex editor program.
        /// </remarks>
        public static void DebugStream(this Stream stream, string hexFormat)
        {
            stream.DebugStream(4, 16, hexFormat);
        }
        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="byteGroup">The cluster of byte values, separated by a space.</param>
        /// <param name="bytesPerRow">the number of bytes each line, separated by a newline.</param>
        /// <remarks>
        /// The format printed in is variable as defined by the parameters. The organization
        /// of data is akin to that of HxD, a popular hex editor program.
        /// </remarks>
        public static void DebugStream(this Stream stream, uint byteGroup, uint bytesPerRow)
        {
            stream.DebugStream(byteGroup, bytesPerRow, "X2");
        }
        /// <summary>
        /// Prints the binary and string representation of a stream to the Console.
        /// </summary>
        /// <param name="stream">The current stream.</param>
        /// <param name="byteGroup">The cluster of byte values, separated by a space.</param>
        /// <param name="bytesPerRow">the number of bytes each line, separated by a newline.</param>
        /// <param name="hexFormat">The display format of the offset indicator.</param>
        /// <remarks>
        /// The format printed in is variable as defined by the parameters. The organization
        /// of data is akin to that of HxD, a popular hex editor program.
        /// </remarks>
        public static void DebugStream(this Stream stream, uint byteGroup, uint bytesPerRow, string hexFormat)
        {
            string temp = null;
            string str = null;
            int count = 0;

            foreach (byte b in stream.StreamToByteArray())
            {
                if (count % bytesPerRow == 0)
                    temp += "0x" + count.ToString("X2") + " - ";

                count++;
                temp += (b.ToString(hexFormat));
                str += (char)b;

                if (count % byteGroup == 0)
                    temp += " ";

                if (count % bytesPerRow == 0)
                {
                    temp += "- [" + str + "]\n";
                    str = null;
                }
            }

            if (str != null)
                temp += " - [" + str + "]\n";
            Console.WriteLine(temp);
        }
    }
}