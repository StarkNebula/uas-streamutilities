﻿using UnityEngine;
using System.Collections;

namespace System.IO
{
    /// <summary>
    /// Unity specific stream utilities.
    /// </summary>
    public static class UnityStreamExtensions
    {
        /// <summary>
        /// Returns the X and Y components of the current vector as a <c>float[]</c>. 
        /// </summary>
        /// <param name="vector3">The current vector.</param>
        /// <remarks>
        /// Floats are returned in order of X, Y, as a <c>float[]</c> with a length of 2.
        /// </remarks>
        public static float[] ToFloatArray(this Vector2 vector2)
        {
            return new float[] { vector2.x, vector2.y };
        }
        /// <summary>
        /// Returns the X, Y, and Z components of the current vector as a <c>float[]</c>. 
        /// </summary>
        /// <param name="vector3">The current vector.</param>
        /// <remarks>
        /// Floats are returned in order of X, Y, Z, as a <c>float[]</c> with a length of 3.
        /// </remarks>
        public static float[] ToFloatArray(this Vector3 vector3)
        {
            return new float[] { vector3.x, vector3.y, vector3.z };
        }
        /// <summary>
        /// Returns the X, Y, Z, and W components of the current vector as a <c>float[]</c>. 
        /// </summary>
        /// <param name="vector3">The current vector.</param>
        /// <remarks>
        /// Floats are returned in order of X, Y, Z, W as a <c>float[]</c> with a length of 4.
        /// </remarks>
        public static float[] ToFloatArray(this Vector4 vector4)
        {
            return new float[] { vector4.x, vector4.y, vector4.z, vector4.w };
        }
        /// <summary>
        /// Returns the position, rotation, and scale as a single <c>float[]</c>.
        /// </summary>
        /// <param name="transform">The current transform.</param>
        /// <param name="useWorldSpace">Whether or not to get local or global transforms.</param>
        /// <remarks>
        /// Returns the X, Y, and Z position components, X, Y, Z, and W rotation components, and X, Y, and Z scale components in that order.
        /// The yielding <c>float[]</c> has a length of 10.
        /// </remarks>
        public static float[] ToFloatArray(this Transform transform, bool useWorldSpace)
        {
            if (useWorldSpace)
                return new float[]
                {
                // Position
                transform.position.x,
                transform.position.y,
                transform.position.z,

                // Rotation as Quaternion
                transform.rotation.x,
                transform.rotation.y,
                transform.rotation.z,
                transform.rotation.w,

                // Scale
                transform.lossyScale.x,
                transform.lossyScale.y,
                transform.lossyScale.z
                };

            else // useLocalSpace
                return new float[]
                {
                // Position
                transform.localPosition.x,
                transform.localPosition.y,
                transform.localPosition.z,

                // Rotation as Quaternion
                transform.localRotation.x,
                transform.localRotation.y,
                transform.localRotation.z,
                transform.localRotation.w,

                // Scale
                transform.localScale.x,
                transform.localScale.y,
                transform.localScale.z
                };
        }
    }
}